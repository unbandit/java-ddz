package GUI.Controller;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import src.*;

import static java.lang.Math.ceil;
import static java.lang.Math.floor;

public class StartMenuController implements Initializable {
	private static DatabaseHandler storage = null;
	private final int itemsPerPage = 20;
	private static String path = "database/";
	private static String URL = "jdbc:sqlite:database/database.db";

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		hotelsId.setCellValueFactory(new PropertyValueFactory<>("Id"));
		hotelsName.setCellValueFactory(new PropertyValueFactory<>("Name"));
		hotelsLon.setCellValueFactory(new PropertyValueFactory<>("Longitude"));
		hotelsLat.setCellValueFactory(new PropertyValueFactory<>("Latitude"));

		metStatId.setCellValueFactory(new PropertyValueFactory<>("Id"));
		metStatName.setCellValueFactory(new PropertyValueFactory<>("Name"));
		metStatLon.setCellValueFactory(new PropertyValueFactory<>("Longitude"));
		metStatLat.setCellValueFactory(new PropertyValueFactory<>("Latitude"));

		busStopId.setCellValueFactory(new PropertyValueFactory<>("Id"));
		busStopName.setCellValueFactory(new PropertyValueFactory<>("Name"));
		busStopLon.setCellValueFactory(new PropertyValueFactory<>("Longitude"));
		busStopLat.setCellValueFactory(new PropertyValueFactory<>("Latitude"));

		statHotelsName.setCellValueFactory(new PropertyValueFactory<>("Name"));
		countOfBusStops.setCellValueFactory(new PropertyValueFactory<>("CountOfBusStops"));

		statMetroName.setCellValueFactory(new PropertyValueFactory<>("Name"));
		countOfHotels.setCellValueFactory(new PropertyValueFactory<>("CountOfHotels"));

		hotelsPagination.setVisible(false);
		metStatPagination.setVisible(false);
		busStopPagination.setVisible(false);
		countOfBusStopPagination.setVisible(false);
		countOfHotelsPagination.setVisible(false);
	}

	@FXML
	private TabPane tabPane;
	@FXML
	private Label databaseName;

	@FXML
	private Tab hotelsTab;
	@FXML
	private TableView<Hotel> hotelsTable;
	@FXML
	private TableColumn<Hotel, Integer> hotelsId;
	@FXML
	private TableColumn<Hotel, String> hotelsName;
	@FXML
	private TableColumn<Hotel, Double> hotelsLon;
	@FXML
	private TableColumn<Hotel, Double> hotelsLat;
	@FXML
	private Pagination hotelsPagination;

	@FXML
	private Tab metStatTab;
	@FXML
	private TableView<MetroStation> metStatTable;
	@FXML
	private TableColumn<Hotel, Double> metStatId;
	@FXML
	private TableColumn<MetroStation, String> metStatName;
	@FXML
	private TableColumn<MetroStation, Double> metStatLon;
	@FXML
	private TableColumn<MetroStation, Double> metStatLat;
	@FXML
	private Pagination metStatPagination;

	@FXML
	private Tab busStopTab;
	@FXML
	private TableView<BusStop> busStopTable;
	@FXML
	private TableColumn<Hotel, Integer> busStopId;
	@FXML
	private TableColumn<BusStop, String> busStopName;
	@FXML
	private TableColumn<BusStop, Double> busStopLon;
	@FXML
	private TableColumn<BusStop, Double> busStopLat;
	@FXML
	private Pagination busStopPagination;

	@FXML
	private Tab countOfBusStopsTab;
	@FXML
	private TableView<Hotel> CountOfBusStopTable;
	@FXML
	private TableColumn<Hotel, String> statHotelsName;
	@FXML
	private TableColumn<Hotel, Integer> countOfBusStops;
	@FXML
	private Pagination countOfBusStopPagination;

	@FXML
	private Tab countOfHotelsTab;
	@FXML
	private TableView<MetroStation> CountOfHotelsTable;
	@FXML
	private TableColumn<MetroStation, String> statMetroName;
	@FXML
	private TableColumn<MetroStation, Integer> countOfHotels;
	@FXML
	private Pagination countOfHotelsPagination;

	@FXML
	private AnchorPane filterPane;
	@FXML
	private TextField filterField;
	@FXML
	private Label labelFilter;
	@FXML
	private Button buttonFilter;



	@FXML
	void reloadDatabase() {
		if (storage == null || storage.isEmpty()) {
			return;
		}

		String selectedTab = tabPane.getSelectionModel().getSelectedItem().getId();

		switch (selectedTab) {
			case "hotelsTab": {
				ArrayList<Hotel> list = storage.getHotels();

				hotelsPagination.setPageFactory(pageIndex -> {
					int fromIndex = pageIndex * itemsPerPage;
					int toIndex = fromIndex + itemsPerPage;
					int maxIndex = (int) floor((double) list.size() / itemsPerPage);

					if (hotelsPagination.getCurrentPageIndex() < maxIndex) {
						hotelsTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, toIndex)));
					} else {
						hotelsTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, fromIndex + (list.size() % itemsPerPage))));
					}
					return hotelsTable;
				});
				hotelsPagination.setPageCount((int) ceil((double) list.size() / itemsPerPage));
				break;
			}
			case "metStatTab": {
				ArrayList<MetroStation> list = storage.getMetroStations();

				metStatPagination.setPageFactory(pageIndex -> {
					int fromIndex = pageIndex * itemsPerPage;
					int toIndex = fromIndex + itemsPerPage;
					int maxIndex = (int) floor((double) list.size() / itemsPerPage);

					if (metStatPagination.getCurrentPageIndex() < maxIndex) {
						metStatTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, toIndex)));
					} else {
						metStatTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, fromIndex + (list.size() % itemsPerPage))));
					}
					return metStatTable;
				});
				metStatPagination.setPageCount((int) ceil((double) list.size() / itemsPerPage));
				break;
			}
			case "busStopTab": {
				ArrayList<BusStop> list = storage.getBusStops();

				busStopPagination.setPageFactory(pageIndex -> {
					int fromIndex = pageIndex * itemsPerPage;
					int toIndex = fromIndex + itemsPerPage;
					int maxIndex = (int) floor((double) list.size() / itemsPerPage);

					if (busStopPagination.getCurrentPageIndex() < maxIndex) {
						busStopTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, toIndex)));
					} else {
						busStopTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, fromIndex + (list.size() % itemsPerPage))));
					}
					return busStopTable;
				});
				busStopPagination.setPageCount((int) ceil((double) list.size() / itemsPerPage));
				break;
			}
			case "countOfBusStopsTab": {
				ArrayList<Hotel> list = storage.getHotelsStatisticFilter(0);

				countOfBusStopPagination.setPageFactory(pageIndex -> {
					int fromIndex = pageIndex * itemsPerPage;
					int toIndex = fromIndex + itemsPerPage;
					int maxIndex = (int) floor((double) list.size() / itemsPerPage);

					if (countOfBusStopPagination.getCurrentPageIndex() < maxIndex) {
						CountOfBusStopTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, toIndex)));
					} else {
						CountOfBusStopTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, fromIndex + (list.size() % itemsPerPage))));
					}
					return CountOfBusStopTable;
				});
				countOfBusStopPagination.setPageCount((int) ceil((double) list.size() / itemsPerPage));
				break;
			}
			case "countOfHotelsTab": {
				ArrayList<MetroStation> list = storage.getMetroStationsStatisticFilter(0);

				countOfHotelsPagination.setPageFactory(pageIndex -> {
					int fromIndex = pageIndex * itemsPerPage;
					int toIndex = fromIndex + itemsPerPage;
					int maxIndex = (int) floor((double) list.size() / itemsPerPage);

					if (countOfHotelsPagination.getCurrentPageIndex() < maxIndex) {
						CountOfHotelsTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, toIndex)));
					} else {
						CountOfHotelsTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, fromIndex + (list.size() % itemsPerPage))));
					}
					return CountOfHotelsTable;
				});
				countOfHotelsPagination.setPageCount((int) ceil((double) list.size() / itemsPerPage));
				break;
			}
		}
	}

	@FXML
	void OpenDatabase() {
		File file;
		FileChooser fileChooser;
		storage = new DatabaseHandler(URL);
		storage.createTables();

		try {
			fileChooser = new FileChooser();
			file = fileChooser.showOpenDialog(new Stage());
		} catch (Exception e) {
			return;
		}
		storage = new DatabaseHandler("jdbc:sqlite:" + file.getPath());
		storage.createTables();
		databaseName.setText(file.getPath());
	}

	@FXML
	void importData() {
		if (storage.isEmpty()) {
			CSVReader csvReader = new CSVReader(path);
			JsonReader jsonReader = new JsonReader(path);
			csvReader.readHotels(storage);
			csvReader.readBusStops(storage);
			jsonReader.readMetroStations(storage);
		}
		reloadDatabase();
		hotelsPagination.setVisible(true);
		metStatPagination.setVisible(true);
		busStopPagination.setVisible(true);
		countOfBusStopPagination.setVisible(true);
		countOfHotelsPagination.setVisible(true);
		databaseName.setStyle("-fx-text-fill: black;");
	}

	@FXML
	void Statistic() {
		if (storage == null || storage.isEmpty()) {
			databaseName.setStyle("-fx-text-fill: red;");
			databaseName.setAlignment(Pos.CENTER);
			databaseName.setText("Error! You did not import data!");
		} else {
			storage.metroStationsStatistic();
			storage.HotelsStatistic();
			countOfBusStops.setCellValueFactory(new PropertyValueFactory<>("CountOfBusStops"));
			countOfHotels.setCellValueFactory(new PropertyValueFactory<>("CountOfHotels"));
		}
	}

	@FXML
	void Filter() {
		filterPane.setVisible(true);

		if (storage == null || storage.isEmpty()) {
			labelFilter.setStyle("-fx-text-fill: red;");
			labelFilter.setAlignment(Pos.CENTER);
			labelFilter.setText("Error! You did not import data!");
			filterField.setVisible(false);
			buttonFilter.setVisible(false);
		} else {
			labelFilter.setStyle("-fx-text-fill: black;");
			labelFilter.setAlignment(Pos.CENTER);
			labelFilter.setText("Write name of hotel:");
			filterField.setVisible(true);
			buttonFilter.setVisible(true);
		}
	}


	@FXML
	public void filter() {
		ArrayList<Hotel> list;
		SingleSelectionModel<Tab> selectionModel = tabPane.getSelectionModel();

		String text = filterField.getText();

		if (text.isEmpty()) {
			return;
		}

		list = storage.getHotelOnName(text);

		if (list == null || list.isEmpty()) {
			labelFilter.setStyle("-fx-text-fill: red;");
			labelFilter.setAlignment(Pos.CENTER);
			labelFilter.setText("Not found!");
			filterField.setVisible(false);
			buttonFilter.setVisible(false);
			return;
		}

		selectionModel.select(hotelsTab);

		hotelsPagination.setPageFactory(pageIndex -> {
			int fromIndex = pageIndex * itemsPerPage;
			int toIndex = fromIndex + itemsPerPage;
			int maxIndex = (int) floor((double) list.size() / itemsPerPage);

			if (hotelsPagination.getCurrentPageIndex() < maxIndex) {
				hotelsTable.setItems(FXCollections.observableArrayList(
						list.subList(fromIndex, toIndex))
				);
			} else {
				hotelsTable.setItems(FXCollections.observableArrayList(
						list.subList(fromIndex, fromIndex + (list.size() % itemsPerPage)))
				);
			}
			return hotelsTable;
		});
		hotelsPagination.setPageCount((int) ceil((double) list.size() / itemsPerPage));
		back();
	}

	@FXML
	void back() {
		filterPane.setVisible(false);
	}


	@FXML
	void exit() {
		Stage primaryStage = (Stage) tabPane.getScene().getWindow();
		primaryStage.close();
	}
}
