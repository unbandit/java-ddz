package src;

public class Hotel {
    public int id;
    public String name;
    public double longitude;
    public double latitude;
    public int countofbusstops;

    public Hotel(int id, String name, double longitude, double latitude) {
        this.id = id;
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
        this.countofbusstops = 0;
    }

    public Hotel(int id, String name, double longitude, double latitude, int countofbusstops) {
        this.id = id;
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
        this.countofbusstops = countofbusstops;
    }


    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public int getCountOfBusStops() {
        return this.countofbusstops;
    }

    public String toString() {
        return "id: " + this.id + "\nname: " + this.name + "\nlongitude: " + this.longitude + "\nlatitude: " + this.latitude + this.countofbusstops + "\n";
    }
}
