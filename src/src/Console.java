package src;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class Console {
    public static void parseData(String path, int tableNum) {
        DatabaseHandler storage = new DatabaseHandler("jdbc:sqlite:database/database.db");
        storage.createTables();
        CSVReader csvReader = new CSVReader(path);
        JsonReader jsonReader = new JsonReader(path);
        Scanner scanner = new Scanner(System.in);
        int choice;

        while (true) {
            System.out.println("Выберите действие:\n" +
                    "1. Импорт данных\n" +
                    "2. Подсчет статистики по количеству гостиниц на каждую станцию метро\n" +
                    "3. Подсчет статистики по количеству маршрутов общественного транспорта на гостиницу\n" +
                    "4. Выход\n" +
                    "5. Фильтрация отелей по имени\n");

            try {
                choice = Integer.parseInt(scanner.next());
            } catch (Exception e) {
                System.err.println("Error, select a digit! : " + e.getMessage());
                continue;
            }

            switch (choice) {
                case 1: {
//                    System.out.println("------------------------HOTELS------------------------\n");
//                    csvReader.readHotels(storage);
//                    System.out.println("------------------------BUSSTOPS------------------------\n");
//                    csvReader.readBusStops(storage);
//                    System.out.println("------------------------METROSTATIONS------------------------\n");
                    jsonReader.readMetroStations(storage);
                    break;
                }
                case 2: {
                    storage.metroStationsStatistic();

                    ArrayList<MetroStation> metroStations = new ArrayList<>();
                    metroStations = storage.getMetroStations();

                    for (MetroStation metroStation : metroStations) {
                        System.out.println(metroStation.getName() + ": " + metroStation.countofhotels);
                    }
                    break;
                }
                case 3: {
                    storage.HotelsStatistic();

                    ArrayList<Hotel> hotels = new ArrayList<>();
                    hotels = storage.getHotels();

                    for (Hotel hotel : hotels) {
                        System.out.println(hotel.getName() + ": " + hotel.countofbusstops);
                    }
                    break;
                }
                case 4: {
                    storage.closeDB();
                    return;
                }
                case 5: {
                    String ch = scanner.next();
                    ArrayList<Hotel> hotels = new ArrayList<>();
                    hotels = storage.getHotelOnName(ch);

                    for (Hotel hotel : hotels) {
                        System.out.println(hotel.toString());
                    }
                    break;
                }
                default: {
                    System.out.print("Wrong number!\n");
                }
            }
        }
    }

    public static void main(String[] args) {
        parseData("database/", 0);
    }
}
