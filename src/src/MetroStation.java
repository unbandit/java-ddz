package src;

public class MetroStation {
    public double id;
    public String name;
    public double longitude;
    public double latitude;
    public int countofhotels;

    public MetroStation(double id, String name, double longitude, double latitude) {
        this.id = id;
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
        this.countofhotels = 0;
    }

    public MetroStation(double id, String name, double longitude, double latitude, int countofhotels) {
        this.id = id;
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
        this.countofhotels = countofhotels;
    }

    public double getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public int getCountOfHotels() {
        return this.countofhotels;
    }

    public String toString() {
        return "id: " + this.id + "\nname: " + this.name + "\nlongitude: " + this.longitude + "\nlatitude: " + this.latitude + this.countofhotels + "\n";
    }
}
