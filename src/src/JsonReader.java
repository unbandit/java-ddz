package src;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class JsonReader {
    private String pathToJsonFile;

    public JsonReader(String pathToJsonFile) {
        this.pathToJsonFile = pathToJsonFile + "/metro.json";
    }

    public void readMetroStations(DatabaseHandler database) {
        String name;
        double id, longitude, latitude;
        JSONParser parser = new JSONParser();

        int i = 0;
        double count = 3.52;
        int var = 0;
        try (Reader reader = Files.newBufferedReader(Paths.get(pathToJsonFile))) {
            Object obj = parser.parse(reader);
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray lines = (JSONArray) jsonObject.get("lines");

            for (Object lineObj : lines) {
                JSONObject line = (JSONObject) lineObj;
                JSONArray stations = (JSONArray) line.get("stations");

                for (Object stationObj : stations) {
                    JSONObject station = (JSONObject) stationObj;

                    id = Double.parseDouble((String) station.get("id"));
                    name = (String) station.get("name");
                    latitude = (Double) station.get("lat");
                    longitude = (Double) station.get("lng");

                    MetroStation metroStation = new MetroStation(id, name, longitude, latitude);

                    database.insertMetroStation(metroStation);
                    i++;
                    if (i % (int) count == 0 && var != 100) {
                        var++;
                        System.out.println("Importing metro stations...   " + var + "%");
                    }
                }
            }
            System.out.println("Import has been completed");
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    private double parseDouble(String number)
    {
        number = number.trim().replaceAll(",", ".").replaceAll("[^0-9.]", "");//.replaceAll("\\s", "");
        number = number.isEmpty() ? "0" : number;

        return Double.parseDouble(number);
    }
}
