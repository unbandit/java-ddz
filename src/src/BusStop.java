package src;

public class BusStop {
    public int id;
    public String name;
    public double longitude;
    public double latitude;

    public BusStop(int id, String name, double longitude, double latitude) {
        this.id = id;
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public String toString() {
        return "id: " + this.id + "\nname: " + this.name + "\nlongitude: " + this.longitude + "\nlatitude: " + this.latitude + "\n";
    }
}
