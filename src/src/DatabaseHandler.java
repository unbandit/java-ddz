package src;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;


public class DatabaseHandler {
    public Connection connection;
    public Statement statement;
    public ResultSet resultSet;
    private PreparedStatement insertHotel;
    private PreparedStatement insertMetroStation;
    private PreparedStatement insertBusStop;

    public DatabaseHandler(String url) {
        try {
            this.connection = DriverManager.getConnection(url);
            this.connection.prepareStatement("PRAGMA encoding = 'UTF-8';");
            this.statement = this.connection.createStatement();
        } catch (SQLException exception) {
            System.out.println("Error! " + exception);
        }

    }

    public void createTables() {
        String createHotelStatement = "CREATE TABLE IF NOT EXISTS hotels" +
                "(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "name TEXT, " +
                "latitude DOUBLE, " +
                "longitude DOUBLE, " +
                "countofbusstops INTEGER " +
                ");";

        String insertHotelStatement = "INSERT INTO hotels " +
                "(id, name, latitude, longitude, countofbusstops) " +
                "VALUES " +
                "(?, ?, ?, ?, ?);";

        String createMetroStationStatement = "CREATE TABLE IF NOT EXISTS metrostations" +
                "(" +
                "id DOUBLE PRIMARY KEY, " +
                "name TEXT, " +
                "latitude DOUBLE, " +
                "longitude DOUBLE, " +
                "countofhotels INTEGER " +
                ");";

        String insertMetroStationStatement = "INSERT INTO metrostations " +
                "(id, name, latitude, longitude, countofhotels) " +
                "VALUES " +
                "(?, ?, ?, ?, ?);";

        String createBusStopStatement = "CREATE TABLE IF NOT EXISTS busstops" +
                "(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "name TEXT, " +
                "latitude DOUBLE, " +
                "longitude DOUBLE " +
                ");";

        String insertBusStopStatement = "INSERT INTO busstops " +
                "(id, name, latitude, longitude) " +
                "VALUES " +
                "(?, ?, ?, ?);";

        try {
            PreparedStatement createHotel = connection.prepareStatement(createHotelStatement);
            createHotel.execute();
            insertHotel = connection.prepareStatement(insertHotelStatement);

            PreparedStatement createMetroStation = connection.prepareStatement(createMetroStationStatement);
            createMetroStation.execute();
            insertMetroStation = connection.prepareStatement(insertMetroStationStatement);

            PreparedStatement createBusStop = connection.prepareStatement(createBusStopStatement);
            createBusStop.execute();
            insertBusStop = connection.prepareStatement(insertBusStopStatement);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public void insertHotel(Hotel hotel) {
        try {
            this.insertHotel.setInt(1, hotel.getId());
            this.insertHotel.setString(2, hotel.getName());
            this.insertHotel.setDouble(3, hotel.getLatitude());
            this.insertHotel.setDouble(4, hotel.getLongitude());
            this.insertHotel.setInt(5, hotel.getCountOfBusStops());
            this.insertHotel.execute();
        } catch (SQLException exception) {
            System.out.println("Write error! " + String.valueOf(exception));
        }
    }

    public void insertMetroStation(MetroStation metroStation) {
        try {
            this.insertMetroStation.setDouble(1, metroStation.getId());
            this.insertMetroStation.setString(2, metroStation.getName());
            this.insertMetroStation.setDouble(3, metroStation.getLatitude());
            this.insertMetroStation.setDouble(4, metroStation.getLongitude());
            this.insertMetroStation.setInt(5, metroStation.getCountOfHotels());
            this.insertMetroStation.execute();
        } catch (SQLException exception) {
            System.out.println("Write error! " + String.valueOf(exception));
        }
    }

    public void insertBusStop(BusStop busStop) {
        try {
            this.insertBusStop.setInt(1, busStop.getId());
            this.insertBusStop.setString(2, busStop.getName());
            this.insertBusStop.setDouble(3, busStop.getLatitude());
            this.insertBusStop.setDouble(4, busStop.getLongitude());
            this.insertBusStop.execute();
        } catch (SQLException exception) {
            System.out.println("Write error! " + String.valueOf(exception));
        }
    }

    private void updateMetroStation(String name, int count) {
        try {
            statement.executeUpdate("UPDATE metrostations SET countofhotels=" +
                    count + " WHERE name=\"" + name + "\";");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void updateHotel(int id, int count) {
        try {
            statement.executeUpdate("UPDATE hotels SET countofbusstops=" +
                    count + " WHERE id=\"" + id + "\";");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Hotel> getHotels() {
        ArrayList<Hotel> hotels = new ArrayList<>();

        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM hotels ORDER BY name;");

            while (resultSet.next()) {
                Hotel hotel = new Hotel(resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getDouble("latitude"),
                        resultSet.getDouble("longitude"),
                        resultSet.getInt("countofbusstops"));
                hotels.add(hotel);
            }

        } catch (SQLException e) {
            System.err.println("Error reading hotels: " + e.getMessage());
        }

        return hotels;
    }

    public ArrayList<BusStop> getBusStops() {
        ArrayList<BusStop> busStops = new ArrayList<>();

        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM busstops ORDER BY name;");

            while (resultSet.next()) {
                BusStop busStop = new BusStop(resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getDouble("latitude"),
                        resultSet.getDouble("longitude"));

                busStops.add(busStop);
            }

        } catch (SQLException e) {
            System.err.println("Error reading bus stops: " + e.getMessage());
        }

        return busStops;
    }

    public ArrayList<MetroStation> getMetroStations() {
        ArrayList<MetroStation> metroStations = new ArrayList<>();

        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM metrostations ORDER BY name;");

            while (resultSet.next()) {
                MetroStation metroStation = new MetroStation(resultSet.getDouble("id"),
                        resultSet.getString("name"),
                        resultSet.getDouble("latitude"),
                        resultSet.getDouble("longitude"),
                        resultSet.getInt("countofhotels"));

                metroStations.add(metroStation);
            }

        } catch (SQLException e) {
            System.err.println("Error reading metro stations: " + e.getMessage());
        }

        return metroStations;
    }

    private static double toRadian(double number) {
        return number * Math.PI / 180;
    }

    private static double getDistance(double x1, double y1, double x2, double y2) {
        double radLat1 = toRadian(y1);
        double radLat2 = toRadian(y2);
        double latDifference = radLat1 - radLat2;
        double lonDifference = toRadian(x1) - toRadian(x2);
        double dist = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(latDifference / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(lonDifference / 2), 2)));

        return dist * 6378137;
    }


    public void metroStationsStatistic() {
        ArrayList<MetroStation> metroStations = getMetroStations();
        ArrayList<Hotel> hotels = getHotels();

        int i = 0;
        double count = 42295.83;
        for (MetroStation metroStation : metroStations) {
            metroStation.countofhotels = 0;
            //updateMetroStation(metroStation.name, 0);
            double maxDistance = 500;
            for (Hotel hotel : hotels) {
                double distance = getDistance(
                        metroStation.getLongitude(),
                        metroStation.getLatitude(),
                        hotel.getLongitude(),
                        hotel.getLatitude()
                );

                if (distance <= maxDistance) {
                    metroStation.countofhotels += 1;
                    updateMetroStation(metroStation.name, metroStation.countofhotels);
                }
                i++;
                if (i % (int) count == 0) {
                    System.out.println("Updating metro stations statistics...   " + i / 42295 + "%");
                }
            }
        }
        System.out.println("Metro stations statistics update is completed");
    }

    public ArrayList<MetroStation> getMetroStationsStatisticFilter(Integer count){
        ArrayList<MetroStation> metroStations = new ArrayList<>();

        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM metrostations" +
                    " WHERE countofhotels > " + count + " ORDER BY name" + ";");

            while (resultSet.next()) {
                MetroStation metroStation = new MetroStation(resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getDouble("latitude"),
                        resultSet.getDouble("longitude"),
                        resultSet.getInt("countofhotels"));
                metroStations.add(metroStation);
            }

        } catch (SQLException e) {
            System.err.println("Error reading hotels: " + e.getMessage());
        }

        return metroStations;
    }

    public boolean isEmpty() {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT 1 FROM hotels LIMIT 1");

            if (resultSet.next()) {
                return false;
            }

            return true;
        } catch (SQLException e) {
            System.err.println("Error checking if the database is empty: " + e.getMessage());
            throw new RuntimeException("Error checking if the database is empty", e);
        }
    }

    public void HotelsStatistic() {
        ArrayList<Hotel> hotels = getHotels();
        ArrayList<BusStop> busStops = getBusStops();

        int i = 0;
        double count = 1436229.87;
        for (Hotel hotel : hotels) {
            hotel.countofbusstops = 0;
            //updateHotel(metroStation.name, 0);
            double maxDistance = 500;
            for (BusStop busStop : busStops) {
                double distance = getDistance(
                        hotel.getLongitude(),
                        hotel.getLatitude(),
                        busStop.getLongitude(),
                        busStop.getLatitude()
                );

                if (distance <= maxDistance) {
                    hotel.countofbusstops += 1;
                    updateHotel(hotel.id, hotel.countofbusstops);
                }
                i++;
                if (i % (int) count == 0) {
                    System.out.println("Updating hotels statistics...   " + i / 1436229 + "%");
                }
            }
        }
        System.out.println("Hotels statistics update is completed");
    }

    public ArrayList<Hotel> getHotelsStatisticFilter(Integer count){
        ArrayList<Hotel> hotels = new ArrayList<>();

        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM hotels" +
                    " WHERE countofbusstops > " + count + " ORDER BY name" + ";");

            while (resultSet.next()) {
                Hotel hotel = new Hotel(resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getDouble("latitude"),
                        resultSet.getDouble("longitude"),
                        resultSet.getInt("countofbusstops"));
                hotels.add(hotel);
            }

        } catch (SQLException e) {
            System.err.println("Error reading hotels: " + e.getMessage());
        }

        return hotels;
    }

    public ArrayList<Hotel> getHotelOnName(String name){
        ArrayList<Hotel> hotels = new ArrayList<>();

        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM hotels" +
                    " WHERE name LIKE '" + name + "%'" + ";");

            while (resultSet.next()) {
                Hotel hotel = new Hotel(resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getDouble("latitude"),
                        resultSet.getDouble("longitude"),
                        resultSet.getInt("countofbusstops"));
                hotels.add(hotel);
            }

        } catch (SQLException e) {
            System.err.println("Error reading hotels: " + e.getMessage());
        }

        return hotels;
    }

    public void closeDB() {
        try {
            this.connection.close();
            this.statement.close();
        } catch (SQLException exception) {
            System.out.println("Error! " + exception);
        }
    }
}
