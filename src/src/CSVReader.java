package src;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CSVReader {
    private String pathToCSVFiles;

    public CSVReader(String pathToCSVFile) {
        this.pathToCSVFiles = pathToCSVFile;
    }

    public void readHotels(DatabaseHandler database) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(pathToCSVFiles + "/hotels.csv"))) {
            String line;
            bufferedReader.readLine();

            int count = 121;
            int i = 0;
            while ((line = bufferedReader.readLine()) != null) {
                try {
                    String[] data = line.split(";");

                    data[4] = data[4].substring(0, 2) + "." + data[4].substring(2);
                    data[5] = data[5].substring(0, 2) + "." + data[5].substring(2);

                    Hotel hotel = new Hotel(
                            Integer.parseInt(data[0].trim()),
                            data[1].trim(),
                            Double.parseDouble(data[4].trim()),
                            Double.parseDouble(data[5].trim())
                    );

                    database.insertHotel(hotel);
                    i++;

                } catch (Exception exception) {
                    continue;
                }
                if (i % count == 0) {
                    System.out.println("Importing hotels...   " + i / 121 + "%");
                }
            }
            System.out.println("Import has been completed");
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void readBusStops(DatabaseHandler database) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(pathToCSVFiles + "/public-transport-stops.csv"))) {
            String line;
            bufferedReader.readLine();

            int i = 0;
            int count = 117;
            while ((line = bufferedReader.readLine()) != null) {
                try {
                    String[] data = line.split(";");

                    data[0] = data[0].substring(1, data[0].length() - 1);
                    data[1] = data[1].substring(1, data[1].length() - 1);
                    data[2] = data[2].substring(1, data[2].length() - 1);
                    data[3] = data[3].substring(1, data[3].length() - 1);

                    BusStop busStop = new BusStop(
                            Integer.parseInt(data[0].trim()),
                            data[1].trim(),
                            Double.parseDouble(data[2].trim()),
                            Double.parseDouble(data[3].trim())
                    );

                    database.insertBusStop(busStop);
                    i++;
                } catch (Exception exception) {
                    System.out.println(exception);
                }
                if (i % count == 0) {
                System.out.println("Importing bus stops...   " + i / 117 + "%");
                }
            }
            System.out.println("Import has been completed");
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private int parseInt(String number)
    {
        number = number.trim().replaceAll("\\D", "");
        number = number.isEmpty() ? "0" : number;

        return Integer.parseInt(number);
    }

    private double parseDouble(String number)
    {
        number = number.trim().replaceAll(",", ".").replaceAll("[^0-9.]", "");//.replaceAll("\\s", "");
        number = number.isEmpty() ? "0" : number;

        return Double.parseDouble(number);
    }
}
